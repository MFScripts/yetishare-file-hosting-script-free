**YetiShare - File Hosting Script Free**

Run your own personal file upload website or a file hosting business and earn money online selling premium file hosting accounts.

**About YetiShare - File Hosting Script Free**

YetiShare File Hosting Script Free is version 2.1 of the premium PHP script available at http://yetishare.com. YetiShare is currently in version 4+ and has undergone a vast amount of changes from the 2.1 version which was released in June 2012.

This community based release to supplied without support or liability, feel free use it as you wish. The only restriction is that this file must remain in place on any installs or redistributed code.

**Release History**

See ___RELEASE_HISTORY.txt for the release history of the file upload script.

**Requirements**

* PHP5.2+
* MySQL
* Mod-Rewrite enabled

**Installation**

Full instructions are given in the ___INSTALLATION.txt file found within the repository.

**Contributions**

If you'd like to contribute to the project, please contact us via the project https://bitbucket.org/MFScripts/yetishare-file-hosting-script-free

**License**

YetiShare File Hosting Script Free is copyrighted by http://mfscripts.com and is released under the http://opensource.org/licenses/MIT. You are free to use YetiShare - File Hosting Script Free in any way that you wish as long as this file remains in place on any installs or redistributed code.

**Support**

This code is released without any support and as-is. Any support requests on this version will be removed. For community driven support please use the project https://bitbucket.org/MFScripts/yetishare-file-hosting-script-free